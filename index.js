const Twitter = require('twitter');
const request = require('request');
const config = require('./config.js');

// Create a Twitter
const twitter = new Twitter(config);

// set filter parameters
const searchTerms = {
    // track: "#apple,#google,#amazon,#facebook,#microsoft",
    // track: "apple,google,amazon,facebook,microsoft",
    track: 'apple,google,amazon,facebook,microsoft',
    language: 'en',
}

// url
const URL = 'http://localhost:3000/tweet';
// const URL = 'http://52.63.208.123:3000/tweet';

// Start the Twitter Stream
twitter.stream('statuses/filter', searchTerms, function(stream) {
    stream.on('data', function(tweet) {
        // let text = tweet.extended_tweet?tweet.extended_tweet.full_text:tweet.full_text?tweet.full_text:tweet.text;
        // let text = tweet.truncated ? tweet.extended_tweet.full_text : tweet.text;
        // console.log(text)
        if (tweet.text != undefined) {
            tweets = {}
            tweets.name = searchTerms;
            tweets.text = (tweet.truncated ? tweet.extended_tweet.full_text : tweet.text).replace(/(?:https?|ftp):\/\/[\n\S]+/g, '');
            request.post({
                url: URL,
                form: { tweet: JSON.stringify(tweet) }
            }, function(error, response, body) {
                if (error) console.log(error);
            });
        }
        
    });
    stream.on('error', function(error) {
        console.log(error);
    });
});