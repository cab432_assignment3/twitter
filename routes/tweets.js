const Twitter = require('twitter');
const config = require('../config');
const request = require('request');

module.exports = (app, io) => {
    // Create a Twitter
    const twitter = new Twitter(config);

    let socketConnection;
    let twitterStream;
    const URL = 'http://localhost:3000/tweet';
    // const URL = 'http://cab432-ELB-364540564.ap-southeast-2.elb.amazonaws.com/tweet';


    app.locals.searchTerm = 'apple'; //Default search term for twitter stream.
    app.locals.showRetweets = false; //Default

    /**
     * Resumes twitter stream.
     */
    const stream = () => {
        console.log('Starting a stream tracking: ' + app.locals.searchTerm);
        twitter.stream('statuses/filter', { track: app.locals.searchTerm,language: 'en'}, (stream) => {
            stream.on('data', (tweet) => {
                // sendMessage(tweet);
                // if ((tweet.text != undefined) && (tweet.text.substring(0,2) != 'RT')) {
                if (tweet.text != undefined) {
                    tweets = {}
                    tweets.name = app.locals.searchTerm;
                    tweets.text = (tweet.truncated ? tweet.extended_tweet.full_text : tweet.text).replace(/(?:https?|ftp):\/\/[\n\S]+/g, '');
                    request.post({
                        url: URL,
                        form: { tweet: JSON.stringify(tweets) }
                    }, function(error, response, body) {
                        if (error) console.log(error);
                    });
                    // console.log(tweets);
                }
            });

            stream.on('error', (error) => {
                console.log(error);
            });

            twitterStream = stream;
        });
    }

    /**
     * Sets search term for twitter stream.
     */
    app.post('/setSearchTerm', (req, res) => {
        let term = req.body.term;
        app.locals.searchTerm = term;
        destroyStream();
        // if (twitterStream){
        //     twitterStream.destroy();
        // }
        setTimeout(stream, 5000); 
        // setTimeout(function(){ return; },5000);
        // stream();
    });

    /**
     * Pauses the twitter stream.
     */
    app.post('/pause', (req, res) => {
        console.log('Pause');
        destroyStream();
        // if (twitterStream)
        // twitterStream.destroy();
    });

    /**
     * Resumes the twitter stream.
     */
    app.post('/resume', (req, res) => {
        console.log('Resume');
        stream();
    });

    //Establishes socket connection.
    io.on("connection", socket => {
        socketConnection = socket;
        console.log("Client connected:", socket.client.id);
        // stream();
        socket.on('disconnect', function() {
            // if (twitterStream) twitterStream.destroy();
            destroyStream();
            console.log("Client disconnected");
        });
    });

    // Destroy twitter stream
    const destroyStream = () => {
        if (twitterStream) twitterStream.destroy();
    }
    // /**
    //  * Emits data from stream.
    //  * @param {String} msg 
    //  */
    // const sendMessage = (msg) => {
    //     socketConnection.emit("tweets", msg);
    // }
};